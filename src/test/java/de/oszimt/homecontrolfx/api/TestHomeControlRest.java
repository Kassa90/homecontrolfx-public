package de.oszimt.homecontrolfx.api;

import de.oszimt.homecontrolfx.api.HomeControlRest;
import javafx.scene.paint.Color;

public class TestHomeControlRest {

	private static void sleep(long time)
	{
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		// New Home Control Server
		IHomeControl myRestServer = new HomeControlRest();
		// Test with localhost
		myRestServer.setServerAddress("http://localhost:8080");
		// Generate the auth key
		String myKey = myRestServer.generateAuthKey("Hallo", "Welt!");
		System.out.println("Received token: " + myKey);
		myRestServer.setAuthKey(myKey);
		if (!myRestServer.checkAuthKey()) {
			System.out.println("Login error - Token:" + myKey);
			return;
		}
		if (!myRestServer.checkLogin("Hallo", "Welt!")) {
			System.out.println("Login error - Token:" + myKey);
			return;		
		}
			
		
		System.out.println("Login ok - User: " + myRestServer.getUserName());
		// Get temp
		double curTemp = myRestServer.getTemperature();
		System.out.println("Temperature is " + curTemp + "°C");

		// Get light state
		boolean curState = myRestServer.getLightState();
		System.out.println("Light is " + (curState ? "on" : "off") + "!");
		System.out.println("Light changed to " + (myRestServer.switchLightState() ? "on" : "off") + "!");
		System.out.println("Light is " + (curState ? "on" : "off") + "!");
		System.out.println("Light changed to " + (myRestServer.switchLightState() ? "on" : "off") + "!");				
		
		
		// Get temp
		curTemp = myRestServer.getTemperature();
		System.out.println("Temperature is " + curTemp + "°C");
		
		// Set temp
		 myRestServer.setWantedTemperature(25);
		myRestServer.setAutoDown(true);
		double wantedTemp = myRestServer.getWantedTemperature();
		boolean acState =  myRestServer.getAutoDown();
		System.out.format("Wanted temperature is %02.2f, A/C is %s...", wantedTemp, (acState ? "running": "off"));
		while (curTemp < 24 || curTemp > 26) {
			curTemp = myRestServer.getTemperature();
			System.out.println("Temperature is " + curTemp + "°C");
			sleep(500);
		}
		 myRestServer.setWantedTemperature(15);
			while (curTemp < 14 || curTemp > 16) {
				curTemp = myRestServer.getTemperature();
				System.out.println("Temperature is " + curTemp + "°C");
				sleep(500);
			}
		
		// Get brightness
		int curBrightness = myRestServer.getBrightness();
		System.out.println("Brightness is " + curBrightness + "!");
		for (int i = 0; i <= 100; i = i + 10) {
			myRestServer.setBrightness(i);
			System.out.println("Brightness changed to " + myRestServer.getBrightness() + "!");
			sleep(100);
		}
		
		// Change color
		myRestServer.setColor(Color.WHITE);
		Color curColor = myRestServer.getColor();
		System.out.format("Current color is #%02X%02X%02X%n", (int) (255 * curColor.getRed()), (int) (255 * curColor.getGreen()), (int) (255 * curColor.getBlue()));
		sleep(1000);
		myRestServer.setColor(Color.BLUEVIOLET);
		curColor = myRestServer.getColor();
		System.out.format("Current color is #%02X%02X%02X%n", (int) (255 * curColor.getRed()), (int) (255 * curColor.getGreen()), (int) (255 * curColor.getBlue()));
		
		// Change display
		myRestServer.setText("Hallo Welt!");
		String currentText = myRestServer.getText();
		System.out.println("Current text ist " + currentText);
		sleep(2000);
		myRestServer.setText("Test erfolgreich!");
		currentText = myRestServer.getText();
		System.out.println("Current text ist " + currentText);
		
	}
}
