package de.oszimt.homecontrolfx.api;

import javafx.scene.paint.Color;

public class TestHomeControlSim {

	public static void main(String[] args) {
		// New Home Control Server
		IHomeControl mySimulatedServer = new HomeControlSim();

		// Generate the auth key
		String myKey = mySimulatedServer.generateAuthKey("Hallo", "Welt!");
		System.out.println("Received token: " + myKey);
		mySimulatedServer.setAuthKey(myKey);
		if (!mySimulatedServer.checkAuthKey()) {
			System.out.println("Login error - Token:" + myKey);
			return;
		}
		System.out.println("Login ok - User: " + mySimulatedServer.getUserName());
		// Get temp
		double curTemp = mySimulatedServer.getTemperature();
		System.out.println("Temperature is " + curTemp + "°C");
		// Get light state
		boolean curState = mySimulatedServer.getLightState();
		System.out.println("Light is " + (curState ? "on" : "off") + "!");
		System.out.println("Light changed to " + (mySimulatedServer.switchLightState() ? "on" : "off") + "!");
		System.out.println("Light is " + (curState ? "on" : "off") + "!");
		System.out.println("Light changed to " + (mySimulatedServer.switchLightState() ? "on" : "off") + "!");				
		// Get temp
		curTemp = mySimulatedServer.getTemperature();
		System.out.println("Temperature is " + curTemp + "°C");
		// Get brightness
		int curBrightness = mySimulatedServer.getBrightness();
		System.out.println("Brightness is " + curBrightness + "!");
		for (int i = 0; i <= 100; i = i + 10) {
			mySimulatedServer.setBrightness(i);
			System.out.println("Brightness changed to " + mySimulatedServer.getBrightness() + "!");
		}
		// Change color
		Color curColor = mySimulatedServer.getColor();
		System.out.format("Current color is #%02X%02X%02X", (int) (255 * curColor.getRed()), (int) (255 * curColor.getGreen()), (int) (255 * curColor.getBlue()));
	}

}
