package de.oszimt.homecontrolfx.gui;

import java.io.IOException;

import de.oszimt.homecontrolfx.api.HomeControlRest;
import de.oszimt.homecontrolfx.api.IHomeControl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LightSettings extends Application {


	// Create a IHomeControl Server
	public final static IHomeControl server = new HomeControlRest();

	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage stage) throws Exception {
		// Set the server address
		server.setServerAddress("http://homecontrol.oszimt.local:8080");
		// Login the Test User
		server.setAuthKey(server.generateAuthKey("Hallo", "Welt!"));
		
		// Show the LightSettings window
		Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource("LightSettings.fxml"));
			Stage newStage = new Stage();
			newStage.setTitle("HomeControl FX - Lichteinstellungen");
			newStage.setScene(new Scene(root));
			newStage.sizeToScene();
			newStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Starts the JavaFX application when class is started
	 * 
	 * @param args
	 *            command line arguments for the application
	 */
	public static void main(String[] args) {
		Application.launch(LightSettings.class);
	}

}