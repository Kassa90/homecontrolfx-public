package de.oszimt.homecontrolfx.gui;

//JafaFX Application
import de.oszimt.homecontrolfx.api.HomeControlSim;
import de.oszimt.homecontrolfx.api.IHomeControl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
//JavaFX Scene
import javafx.scene.Parent;
import javafx.scene.Scene;
//JafaFX Stage
import javafx.stage.Stage;

/**
* <p>Lädt die FXML-Datei <i>LoginWindow.fxml</i> und zeigt sie in einem Fenster an.</p>
* 
* <p>Diese Klasse sollte <b style="color:red">nicht</b> verändert werden!</p>
* 
* @author M. Schleyer
*
*/
public class LoginWindow extends Application {


	/*
	 * (non-Javadoc)
	 * 
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	
	// Create a IHomeControl Server 
	public final static IHomeControl server = new HomeControlSim();
	
	@Override
	public void start(Stage stage) throws Exception {
		// The main application
		// Load the FMXL file
		FXMLLoader myLoader = new FXMLLoader(getClass().getResource("LoginWindow.fxml"));
		Parent root = myLoader.load();
		// Get access to the controller - e.g. to attach the controller
		LoginWindowController myController = (LoginWindowController) myLoader.getController();
		// Create the JavyFX scene and attache it to the stage
		Scene scene = new Scene(root);
		stage.setScene(scene);
		// Set the window title
		stage.setTitle("HomeControl FX - Anmelden");
		// Show the stage / window
		stage.show();
	}

	/**
	 * Starts the JavaFX application when class is started
	 * 
	 * @param args
	 *            command line arguments for the application
	 */
	public static void main(String[] args) {
		Application.launch(LoginWindow.class);
	}

}
