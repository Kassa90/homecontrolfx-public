package de.oszimt.homecontrolfx.gui;

import de.oszimt.homecontrolfx.api.UserLogin;

/**
 * <p>
 * Vorlage für den Controller. In dieser Klasse werden alle Events definiert.
 * Events sind die Aktionen, die vom Benutzer ausgelöst werden können. Dazu
 * gehört zum Beispiel der Klick auf einen Button.
 * </p>
 * 
 * @author Martin Schleyer
 *
 */
public class LoginWindowController {

	/**
	 * Erstellt ein Objekt der UserLogin-Klasse mit Test-Benutzerdaten.
	 * 
	 * @see de.oszimt.homecontrolfx.api.UserLogin
	 */
	@SuppressWarnings("unused")
	private UserLogin userLogin = new UserLogin("Hallo", "Welt!");

	/**
	 * Hier kommen die weiteren Attribute des Controllers hin. Sie können sich
	 * ein Beispiel dazu direkt im SceneBuilder anschauen.
	 * 
	 * Wählen Sie dazu "View" &gt;&gt; "Show Sample Controller Skeleton" im Menü
	 * aus, während Sie die Datei <i>LoginWindow.fxml</i> bearbeiten.
	 */

	// Attribute...

	/**
	 * Anschließend folgen die einzelnen Events, die Sie im SceneBuilder
	 * definiert haben. Beispiel:
	 * 
	 *  protected void btnHalloWeltClick(){
	 *       System.out.println("Hallo Welt!"); }
	 */

	// Methoden ...
}
