package de.oszimt.homecontrolfx.api;

import javafx.scene.paint.Color;

/**
 * Interface to the HomeControlFX Server. Offers authentication methods and
 * access to light, brightness and temperature.
 * 
 * @author M. Schleyer (schleyer@oszimt.de)
 *
 */
public interface IHomeControl {
	
	/////////////////////////////////////////////////////////////
	// Server Connection
	/**
	 * Get the server address as URL
	 * 
	 * @return the address of the current server
	 */
	public String getServerAddress();
	
	/**
	 * Set the server addressa s URL
	 * 
	 * @param _address URL of the server, e.g. "http://1.2.3.4:8080"
	 */
	public void setServerAddress(String _address);
	
	

	/////////////////////////////////////////////////////////////
	// Auth
	/**
	 * Sets the AUTH token for the application to be used from now on.
	 * <p>
	 * the key is not checked for validity, use <code>checkAuthKey</code> to do
	 * so
	 * 
	 * @param key
	 *            AUTH token to be used from now on
	 */
	public void setAuthKey(String key);

	/**
	 * Generates an AUTH token using an user name and a password. This token can
	 * be passed to <code>setAuthKey</code>.
	 * <p>
	 * Please note that the generated token is not set to the interface, this
	 * needs to be done explicitly with <code>setAuthKey</code>
	 * 
	 * @param username
	 *            user name for the new AUTH token
	 * @param password
	 *            password for the new AUTH token
	 * @return the newly generated AUTH token
	 * @see setAuthKey
	 */
	public String generateAuthKey(String username, String password);

	/**
	 * checks if the current AUTH token is accepted by the server
	 * 
	 * @return <code>true</code> if the AUTH token was accepted,
	 *         <code>false</code> otherwise
	 * @see setAuthKey
	 */
	public boolean checkAuthKey();
	
	
	/**
	 * generates a token for a user and checks it immediately 
	 * 
	 * @param username
	 *            user name for login
	 * @param password
	 *            password for login

	 * @return <code>true</code> if the login data was accepted,
	 *         <code>false</code> otherwise
	 */
	public boolean checkLogin(String username, String password);
	
	/////////////////////////////////////////////////////////////
	// Light control
	/**
	 * set the light state to the given value
	 * 
	 * @param state
	 *            state of the light as boolean expression
	 * @see getLightState
	 */
	public void setLightState(boolean state);

	/**
	 * reads the current light state from the server
	 * 
	 * @return the light state (<code>true</code> = ON, <code>false</code> =
	 *         OFF)
	 */
	public boolean getLightState();

	/**
	 * switches the light state (either ON to OFF or OFF to ON)
	 * 
	 * @return state of the light
	 * @see getLightState
	 */
	public boolean switchLightState();

	/**
	 * returns the current brightness value
	 * 
	 * @return current brightness (between 0 and 100)
	 */
	public int getBrightness();

	/**
	 * returns the current brightness value
	 * 
	 * @param brightness
	 *            brightness (between 0 and 100)
	 */
	public void setBrightness(int brightness);
	
	/**
	 * returns  the color of the ambient light
	 * 
	 * @return color	Color object
	 * 
	 */
	public Color getColor();

	/**
	 *  sets the current color of the ambient light
	 * 
	 * @param color Color object
	 */
	public void setColor(Color color);

	
	/////////////////////////////////////////////////////////////
	// Temperature control
	/**
	 * returns the current temperature value
	 * 
	 * @return current temperature in degree Celsius
	 */
	public double getTemperature();
	
	/**
	 * sets the wanted temperature of the heating
	 * 
	 * @param temperature wanted temperature in degree celsius
	 */
	public void setWantedTemperature(double temperature);
	/**
	 * returns the wanted temperature of the heating
	 * 
	 * @return wanted temperature in degree celsius
	 */
	public double getWantedTemperature();
	
	
	/**
	 * enables or disables the a/c control
	 * 	  
	 * @param autodown true if a/c control should be active
	 */
	public void setAutoDown(boolean autodown);
	/**
	 * returns the current a/c control state
	 * 
	 * @return true if climate control is active, @alse otherwise
	 */
	public boolean getAutoDown();
	
	
	/////////////////////////////////////////////////////////////
	// Display
	/**
	 * returns the current text shown on display
	 * 
	 * @return current text on unit display
	 */
	public String getText();
	/**
	 * sets text to display on the SmartHome control unit
	 * 
	 * @param text Text to show, max. 32 characters. Any additional text will be cropped
	 */
	public void setText(String text);
	
	
	/////////////////////////////////////////////////////////////
	// User and Server Info
	/**
	 * gets the user login as currently used to login to the server
	 * 
	 * @return the user login as obtained from the server
	 */
	public String getUserLogin();

	/**
	 * gets the full user name of the user
	 * 
	 * @return the full login name as obtained from the server
	 */
	public String getUserName();

	/**
	 * checks if the connection to the server is present
	 * 
	 * @return the connection state (true if connected)
	 */
	public boolean isConnected();
	
	/**
	 * returns the current API version, if server is connected,
	 * null API is disconnected
	 * 
	 * @return API version (e.g. "0.0.1")
	 */
	public String getApiVersion();

}
