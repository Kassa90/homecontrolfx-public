package de.oszimt.homecontrolfx.api;

/**
 * 
 * <p>UserLogin stellt eine API zur Überprüfung der Zugangsdaten bereit.
 * Zu Testzwecken kann ein Benutzername und ein Passwort über den Konstruktor
 * festgelegt werden. Das Passwort wird intern als SHA256-Hash abgelegt und kann
 * daher nicht mehr als Klartext ausgelesen werden.</p>
 * 
 * <p>Siehe auch <a href="http://de.wikipedia.org/wiki/SHA-2">SHA-2</a></p>
 * 
 * @author Martin Schleyer <i>schleyer@oszimt.de</i>
 * @version 0.1
 *
 */
public class UserLogin {

	private String userName;
	private String userHash;
	
	private boolean debug = false;

	/**
	 * Erstellt eine Instanz von UserLogin mit Test-Benutzerdaten für einen
	 * lokalen Test (nicht für den Produktiv-Einsatz!)
	 * 
	 * @param userName      Benutzername, der als gültig erkannt werden soll
	 * @param userPassword	Passwort, das als gültig erkannt werden soll
	 */
	public UserLogin(String userName, String userPassword) {
		this.userName = userName;
		this.userHash = Hash.sha256(userPassword);
	}
	
	/**
	 * Erstellt eine Instanz von UserLogin mit Test-Benutzerdaten für einen
	 * lokalen Test (nicht für den Produktiv-Einsatz!). Zusätzlich kann ein
	 * debug-Modus mit zusätzlichen Ausgaben gestartet werden.
	 * 
	 * @param userName      Benutzername, der als gültig erkannt werden soll
	 * @param userPassword	Passwort, das als gültig erkannt werden soll
	 * @param debug		    Aktiviert zusätzliche debug-Informationen
	 */
	public UserLogin(String userName, String userPassword, boolean debug) {
		this.userName = userName;
		this.userHash = Hash.sha256(userPassword);
		this.debug = debug;
	}


	/**
	 * Startet den Login-Vorgang und überprüft die Benutzerdaten. Im lokalen Test-Modus
	 * wird nur die Gültigkeit der Benutzerdaten überprüft.
	 * 
	 * @param _name		Benutzername, der überprüft werden soll
	 * @param _password	Passwort, das überprüft werden solll
	 * @return	wahr, wenn die Benutzerdaten korrekt waren.
	 */
	public boolean checkLogin(String _name, String _password) {
		// check user name && password
		if (!_name.equalsIgnoreCase(this.userName)) {
			showDebug("Username " + _name + "ist falsch!");
			return false;
		}
		if (!Hash.sha256(_password).equals(this.userHash)) {
			showDebug("Password " + _password + "ist falsch!");
			return false;
		}
		showDebug("Login ok.");
		return true;
	}

	private void showDebug(String string) {
		// TODO Auto-generated method stub
		if (debug)
			System.out.println(string);		
	}

	/**
	 * Gibt den zu Testzwecken gesetzten Benutzername zurück
	 * 
	 * @return der gültige Benutzername
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Übersetzt ein Passwort mit dem intern verwendeten SHA256-Algorithmus in den passenden
	 * Hash-Wert
	 * 
	 * @param userPassword	Passwort, das in einen SHA256-Hashwert umgewandelt werden soll
	 * @return  			der SHA265-Hash des übergebenen Passworts
	 */
	public String toHash(String userPassword) {
		return Hash.sha256(userPassword);
	}

	/**
	 * Setzt zu Testzwecken ein neues Passwort für den Testbenutzer
	 * 
	 * @param userPassword	Setzt ein neues Passwort und legt es als Hash ab
	 */
	public void setPassword(String userPassword) {
		this.userHash = Hash.sha256(userPassword);
	}

	/**
	 * Gibt das Test-Passwort als Hash zurück
	 * 
	 * @return gibt das als SHA256-Hash gespeicherte Passwort zurück
	 */
	public String getUserHash() {
		return userHash;
	}

}
