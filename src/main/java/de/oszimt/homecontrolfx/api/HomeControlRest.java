/**
 * 
 */
package de.oszimt.homecontrolfx.api;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import javafx.scene.paint.Color;

/**
 * Connects to an HomeControlFx server using an HTTP REST-API. The server
 * normally expects authentication, so make sure to set a proper AUTH key.
 * 
 * @author M. Schleyer
 *
 */
public class HomeControlRest implements IHomeControl {

	private String serverAddress = "http://localhost:8080";

	private String authKey;
	private String userName;
	private String userLogin;

	// Light API
	private boolean lightState = false;
	private int brightness = 50;
	private Color lightColor = Color.web("#ffffff");

	// Temperature API
	private double temperature = 23.0;
	private boolean autoDown = false;
	private double temperatureWanted = 23.0;

	// Text API
	private String displayText;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.oszimt.homeautomation.api.IHomeControl#setAuthKey(java.lang.String)
	 */
	@Override
	public void setAuthKey(String key) {
		this.authKey = key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#generateAuthKey(java.lang.
	 * String , java.lang.String)
	 */
	@Override
	public String generateAuthKey(String username, String password) {
		// Request an auth key
		String newKey = "";
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/api/token/{user}/{pw}")
					.routeParam("user", username).routeParam("pw", password).asJson();

			switch (jsonResponse.getStatus()) {
			case 200:
				newKey = jsonResponse.getBody().getObject().getString("token");
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
		return newKey;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#setLightState(boolean)
	 */
	@Override
	public void setLightState(boolean state) {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/light/set/state/{state}")
					.routeParam("state", (state ? "on" : "off")).queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.lightState = jsonResponse.getBody().getObject().getString("state").equalsIgnoreCase("on");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#getLightState()
	 */
	@Override
	public boolean getLightState() {
		// Send the query
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/light/get/")
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.lightState = jsonResponse.getBody().getObject().getString("state").equalsIgnoreCase("on");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
		return this.lightState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#switchLightState()
	 */
	@Override
	public boolean switchLightState() {
		this.setLightState(!this.getLightState());
		return this.lightState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#getTemperature()
	 */
	@Override
	public double getTemperature() {
		// Send the query
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/temp/get/")
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.temperatureWanted = jsonResponse.getBody().getObject().getDouble("wanted");
				this.temperature = jsonResponse.getBody().getObject().getDouble("temperature");
				this.autoDown = jsonResponse.getBody().getObject().getString("autodown").equalsIgnoreCase("on");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
		return this.temperature;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#checkAuthKey()
	 */
	@Override
	public boolean checkAuthKey() {
		// Check the auth key
		boolean keyCheck = false;
		// Send the query
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/api/check")
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.userLogin = jsonResponse.getBody().getObject().getString("user");
				this.userName = jsonResponse.getBody().getObject().getString("name");
				keyCheck = true;
			}
		} catch (UnirestException e) {
			e.printStackTrace();
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
		return keyCheck;
	}

	/**
	 * reads the server address to which the REST-API should connect to
	 * 
	 * @return the URL of the current HomeControlFX server
	 */
	public String getServerAddress() {
		return serverAddress;
	}

	/**
	 * Sets the URL to the HomeControlFx server, e.g.
	 * http://homecontrolfx.local:8080/
	 * 
	 * @param serverAddress
	 *            the URL of the HomeControlFX Server
	 */
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getUserName()
	 */
	@Override
	public String getUserName() {
		checkAuthKey();
		return userName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getUserLogin()
	 */
	@Override
	public String getUserLogin() {
		checkAuthKey();
		return userLogin;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#isConnected()
	 */
	@Override
	public boolean isConnected() {
		return this.checkAuthKey();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getBrightness()
	 */
	@Override
	public int getBrightness() {
		// Send the query
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/light/get/")
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.brightness = (int) Math.floor(jsonResponse.getBody().getObject().getInt("dim") / 2.55);
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
		return this.brightness;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#setBrightness(int)
	 */
	@Override
	public void setBrightness(int brightness) {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/light/set/brightness/{value}")
					.routeParam("value", String.format("%d", (int) Math.min(255, Math.max(0, brightness * 2.55))))
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.brightness = (int) Math.floor(jsonResponse.getBody().getObject().getInt("dim") / 2.55);
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
	}

	@Override
	public Color getColor() {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/light/get/")
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.lightColor = Color.web(jsonResponse.getBody().getObject().getString("color"));
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
		return this.lightColor;

	}

	private String getColorAsWebcolor(Color _color) {
		return String.format("%02X%02X%02X", (int) (255 * _color.getRed()), (int) (255 * _color.getGreen()),
				(int) (255 * _color.getBlue()));
	}

	@SuppressWarnings("unused")
	private String getColorAsWebcolor() {
		return getColorAsWebcolor(this.lightColor);
	}

	@Override
	public void setColor(Color color) {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/light/set/color/{value}")
					.routeParam("value", this.getColorAsWebcolor(color)).queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.lightColor = Color.web(jsonResponse.getBody().getObject().getString("color"));
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
	}

	@Override
	public String getText() {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/display/get/")
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.displayText = jsonResponse.getBody().getObject().getString("text");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
		return this.displayText;

	}

	@Override
	public void setText(String text) {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/display/set/text/{value}")
					.routeParam("value", text).queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.displayText = jsonResponse.getBody().getObject().getString("text");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
	}

	@Override
	public String getApiVersion() {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/api/").asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.displayText = jsonResponse.getBody().getObject().getString("version");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
		return this.displayText;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.oszimt.homecontrolfx.api.IHomeControl#setWantedTemperature(double)
	 */
	@Override
	public void setWantedTemperature(double temperature) {
		try {
			String tempString  = String.format("%d.%02d", (int) Math.floor(temperature), (int) (100 * (temperature - Math.floor(temperature))));
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/temp/set/wanted/{value}")
					.routeParam("value", tempString)
					.queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.temperatureWanted = jsonResponse.getBody().getObject().getDouble("wanted");
				this.temperature = jsonResponse.getBody().getObject().getDouble("temperature");
				this.autoDown = jsonResponse.getBody().getObject().getString("autodown").equalsIgnoreCase("on");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getWantedTemperature()
	 */
	@Override
	public double getWantedTemperature() {
		this.getTemperature();
		return this.temperatureWanted;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#setAutoDown(boolean)
	 */
	@Override
	public void setAutoDown(boolean autodown) {
		try {
			HttpResponse<JsonNode> jsonResponse = Unirest.get(serverAddress + "/temp/set/autodown/{state}")
					.routeParam("state", (autodown ? "on" : "off")).queryString("token", this.authKey).asJson();
			switch (jsonResponse.getStatus()) {
			case 200:
				this.temperatureWanted = jsonResponse.getBody().getObject().getDouble("wanted");
				this.temperature = jsonResponse.getBody().getObject().getDouble("temperature");
				this.autoDown = jsonResponse.getBody().getObject().getString("autodown").equalsIgnoreCase("on");
				break;
			case 400:
				if (jsonResponse.getBody().getObject().getString("reason").equals("noauth")) {
					System.out.println("[ERROR] Please check auth token");
				}
			}
		} catch (UnirestException e) {
			System.out.format("[ERROR] Failed to connect to server %s!%n", serverAddress);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getAutoDown()
	 */
	@Override
	public boolean getAutoDown() {
		this.getTemperature();
		return this.autoDown;
	}
	
	/* (non-Javadoc)
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#checkLogin(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean checkLogin(String username, String password) {
		this.setAuthKey(this.generateAuthKey(username, password));
		return this.checkAuthKey();
	}
}
