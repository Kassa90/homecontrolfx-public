package de.oszimt.homecontrolfx.api;

import javafx.scene.paint.Color;

/**
 * Simulates an HomeControlFx server for local development. Uses console output
 * to show the server state, authentication is implemented but not required.
 * 
 * @author M. Schleyer
 *
 */
public class HomeControlSim implements IHomeControl {

	private String authKey;
	private boolean lightState = false;
	private Color lightColor = Color.web("#0000CC");
	private double temperature = 23.0;
	private double wantedTemperature = 20;
	private boolean autoDown = false;
	private int brightness = 50;
	private String displayText = "";
	private final String apiVersion = "0.0.2";
	private String serverAddress = "http://1.2.3.4:8080";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.oszimt.homeautomation.api.IHomeControl#setAuthKey(java.lang.String)
	 */
	@Override
	public void setAuthKey(String key) {
		this.authKey = key;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#generateAuthKey(java.lang.
	 * String , java.lang.String)
	 */
	@Override
	public String generateAuthKey(String username, String password) {
		System.out.println("[INFO] Key is generated: " + Hash.sha256(username + ":" + password));
		return Hash.sha256(username + ":" + password);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#setLightState(boolean)
	 */
	@Override
	public void setLightState(boolean state) {
		System.out.println("[INFO] Light switched to " + (state ? "on" : "off"));
		this.lightState = state;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#getLightState()
	 */
	@Override
	public boolean getLightState() {
		System.out.println("[INFO] Light state requested, state is " + (this.lightState ? "on" : "off"));
		return this.lightState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#switchLightState()
	 */
	@Override
	public boolean switchLightState() {
		// TODO Auto-generated method stub
		this.lightState = !this.lightState;
		System.out.println("[INFO] Light state switched, state is " + (this.lightState ? "on" : "off"));
		return this.lightState;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homeautomation.api.IHomeControl#getTemperature()
	 */
	@Override
	public double getTemperature() {
		this.temperature = this.temperature + (Math.random() - 0.5);
		System.out.println("[INFO] Temperature requested, state is " + this.temperature + "°C");
		return this.temperature;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#checkAuthKey()
	 */
	@Override
	public boolean checkAuthKey() {
		// Check the auth key :-)
		boolean keyCheck = this.authKey.equals(generateAuthKey("Hallo", "Welt!"));
		System.out.println("[INFO] Key is checked, is  " + (keyCheck ? "ok" : "invalid") + "!");
		return keyCheck;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getUserLogin()
	 */
	@Override
	public String getUserLogin() {
		return "Hallo";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getUserName()
	 */
	@Override
	public String getUserName() {
		return "Test-Benutzer";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#isConnected()
	 */
	@Override
	public boolean isConnected() {
		// Always assume to be connected if simulated :-)
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getBrightness()
	 */
	@Override
	public int getBrightness() {
		System.out.println("[INFO] Light brightness requested, brightness is " + this.brightness);
		return this.brightness;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#setBrightness(int)
	 */
	@Override
	public void setBrightness(int brightness) {
		this.brightness = Math.min(100, Math.max(0, brightness));
		System.out.println("[INFO] Light brightness changed, brightness is " + this.brightness);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getColor()
	 */
	@Override
	public Color getColor() {
		System.out.format("[INFO] Light color requested, value is #%02X%02X%02X%n",
				(int) (255 * this.lightColor.getRed()), (int) (255 * this.lightColor.getGreen()),
				(int) (255 * this.lightColor.getBlue()));
		return this.lightColor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.oszimt.homecontrolfx.api.IHomeControl#setColor(javafx.scene.paint.
	 * Color)
	 */
	@Override
	public void setColor(Color color) {
		this.lightColor = color;
		System.out.format("[INFO] Light color changed, new value is #%02X%02X%02X%n",
				(int) (255 * this.lightColor.getRed()), (int) (255 * this.lightColor.getGreen()),
				(int) (255 * this.lightColor.getBlue()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getText()
	 */
	@Override
	public String getText() {
		System.out.format("[INFO] Display text requested, value is \"%s\"%n", this.displayText);
		return this.displayText;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#setText(java.lang.String)
	 */
	@Override
	public void setText(String text) {
		this.displayText = text.substring(0, 31);
		System.out.format("[INFO] Display text changed, new value is \"%s\"%n", this.displayText);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.oszimt.homecontrolfx.api.IHomeControl#getApiVersion()
	 */
	@Override
	public String getApiVersion() {
		System.out.format("[INFO] API version requested, value is \"%s\"%n", this.apiVersion);
		return this.apiVersion;
	}

	@Override
	public void setWantedTemperature(double temperature) {
		this.wantedTemperature = Math.max(0, Math.min(temperature, 35));
		System.out.format("[INFO] Wanted temperature set, new value is %2.2f °C%n", this.wantedTemperature);
	}

	@Override
	public double getWantedTemperature() {
		System.out.format("[INFO] Wanted temperature requested, value is %2.2f °C%n", this.wantedTemperature);
		return this.wantedTemperature;
	}

	@Override
	public void setAutoDown(boolean autodown) {
		this.autoDown = autodown;
		System.out.format("[INFO] A/C state changed, new state is %s%n", (this.autoDown ? "ON" : "OFF"));

	}

	@Override
	public boolean getAutoDown() {
		System.out.format("[INFO] A/C state requested, state is %s%n", (this.autoDown ? "ON" : "OFF"));
		return this.autoDown;
	}

	@Override
	public boolean checkLogin(String username, String password) {
		this.setAuthKey(this.generateAuthKey(username, password));
		return this.checkAuthKey();
	}

	@Override
	public String getServerAddress() {
		System.out.format("[INFO] SIMULATED server address requested, URL is \"%s\"%n", this.serverAddress);
		return this.serverAddress;
	}

	@Override
	public void setServerAddress(String _address) {
		this.serverAddress = _address;
		System.out.format("[INFO] SIMULATED server address set to \"%s\"%n", this.serverAddress);
	}
}
